import threading
import time
from datetime import datetime
import os
import json
import pandas as pd
import math
from collections import Counter
import sys

class product_event():
    def __init__(self, data_dir, db_con):
        self.__data_dir = data_dir + '\\{}'
        self.db_conn = db_con
        self.__obj_name = "PRODUCTEVENT"
        self.ctr_cols = ['timestamp', 'datetime', 'codeColor']
        self.period_cols = ['nr_interval', 'start_unix', 'end_unix', 'start_time', 'end_time']
        self.interval = 10
        self.device_type = {}
        self.device_type = self.get_devicetypes()
        self.ext_mapper = {'JSON': self.json_parser}
        self.gmt_mapper = {'JSON': 0, 'DB': -3}
        self.file_set = {device: list(self.__load_fset(device)) for device in self.device_type}

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        while True:
            for device in self.device_type:
                for file in self.__load_fset(device):
                    if file in self.file_set:
                        continue
                    else:
                        self.file_set[device].append(file)
                time.sleep(self.interval)

    def get_devicetypes(self):
        if not self.device_type:
            cursor = self.db_conn.cursor()
            device_types = cursor.execute("SELECT DISTINCT device_type FROM orders").fetchall()
            self.device_type = {device[0] for device in device_types}
        return self.device_type

    @staticmethod
    def __create_periods(start_time, end_time, aggregation):
        agg_seconds = aggregation * 60
        unix_dif = end_time - start_time
        unix_dif = math.ceil(unix_dif / 60 / aggregation)
        for i in range(unix_dif):
            start_unix = start_time + (agg_seconds*(i))
            end_unix = start_time + (agg_seconds*(i+1))
            start_dtime = datetime.fromtimestamp(start_unix)
            end_dtime = datetime.fromtimestamp(end_unix)
            yield (i, start_unix, end_unix, start_dtime, end_dtime)

    @staticmethod
    def __parse_filename(filepath):
        try:
            filename = filepath.name
            fsplit = filename.split(".")
            fpath = filepath.path
            fext = fsplit[1]
            fsplit = fsplit[0].split('_')
            fobj = fsplit[0]
            fdate = fsplit[1]
        # Not the files we are looking for.
        except Exception:
            return (None,)*4
        try:
            fdate = datetime.strptime(fdate, '%Y%m%d')
        except ValueError:
            # Not a valid date format, what you guys thinking
            return (None, )*4
        return fobj.upper(), fpath, fdate, fext.upper()

    def json_parser(self, file, start_time, end_time, product):
        format_gmt = self.gmt_mapper['JSON']
        db_gmt = self.gmt_mapper['DB']
        gmt = format_gmt - db_gmt
        with open(file, 'r') as fread:
            data = json.load(fread)
            for events in data:
                event = events['events'][0]
                event_type = event['event_type']
                if not event_type == 'custom_event':
                    continue

                data = event['data']
                if product and data['custom_attributes']['codeColor'] != product:
                    continue

                timestamp = int(data['timestamp_unixtime_ms']) / 1000.0
                # Converting to database gmt
                timestamp = timestamp - (gmt * 60 * 60)
                data['timestamp_unixtime_ms'] = timestamp
                if not data['timestamp_unixtime_ms'] >= start_time <= end_time:
                    continue

                yield (timestamp, datetime.fromtimestamp(timestamp), data['custom_attributes']['codeColor'])

    def __load_fset(self, platform):
        with os.scandir(self.__data_dir.format(platform)) as dir:
            for file in dir:
                if file.is_dir() and file.name.upper() not in map(str.upper, self.device_type):
                    continue

                fobj, fpath, fdate, fext = product_event.__parse_filename(file)
                # If has a valid file name format, if it is of a processable extension and if correct object type
                if not fpath:
                    continue
                if fext not in self.ext_mapper.keys():
                    continue
                if not fobj == self.__obj_name:
                    continue

                yield (fpath, datetime.timestamp(fdate), fext)

    def __load_data(self, start_unix, end_unix, product, platform):
        for file, date_ref, ext in self.file_set[platform]:
            if start_unix >= date_ref <= end_unix:
                iter_data = self.ext_mapper[ext](file, start_unix, end_unix, product)
                for event in iter_data:
                    yield event

    def get_ctr(self, start_time, end_time, aggregation, product=None, device_type=None):
        aggregation = int(aggregation)
        args = (start_time, end_time, aggregation, product, device_type)
        if not device_type:
            for device_type in self.device_type:
                data = self.__get_ctr(*args[:-1], device_type=device_type)
                if data.empty:
                    continue
                yield device_type, data

    def __get_ctr(self, start_time, end_time, aggregation, product=None, device_type=None):
        date_fmt = '%Y-%m-%d %H:%M:%S'
        args = (start_time, end_time, aggregation, product, device_type)
        start_unix = datetime.strptime(start_time, date_fmt)
        start_unix = datetime.timestamp(start_unix)
        end_unix = datetime.strptime(end_time, date_fmt)
        end_unix = datetime.timestamp(end_unix)

        data = self.__load_data(start_unix, end_unix, product, device_type)
        df_period = pd.DataFrame(self.__create_periods(start_unix, end_unix, aggregation)
                                 , columns=self.period_cols)
        pur_cols, purchases = self.get_purchases(*args)
        purchases = pd.DataFrame.from_records(purchases, columns=pur_cols)
        df_period.set_index(pd.IntervalIndex
                            .from_arrays(df_period['start_unix'],
                                        df_period['end_unix'],
                                        closed='right'), inplace=True)
        count = Counter()
        records = []
        for event in data:
            unix_tmstmp, date_tmstmp, product = event
            try:
                interval = df_period.loc[unix_tmstmp].at['nr_interval']
            #Event is outside of period bounds
            except KeyError:
                continue

            records.append((device_type, interval, product))
        count.update(records)
        if not count:
            return pd.DataFrame([])
        event_cols = ['device_type', 'nr_interval', 'code_color', 'view_cnt']
        df_events = pd.DataFrame.from_records([(device_type, nr_interval, code_color, value)
                                               for (device_type, nr_interval, code_color), value
                                               in count.items()]
                                              , columns= event_cols)
        ctr = pd.merge(df_events, purchases, how="left", on=["device_type", "nr_interval", "code_color"])
        ctr = pd.merge(ctr, df_period, how='left', on="nr_interval")[["start_time", "device_type", "code_color", "view_cnt", "nr_orders"]]
        ctr.nr_orders.fillna(0,inplace=True)
        ctr["ctr_ratio"] = ctr["nr_orders"] / (ctr["view_cnt"] *1.0)
        ctr = ctr[ctr["ctr_ratio"] > 0]
        ctr.start_time.astype(str, inplace=True)
        return ctr[["start_time", "device_type", "code_color", "ctr_ratio"]].round(4)

    def get_purchases(self, start_time, end_time, aggregation, product, device_type):
        query = "SELECT * FROM tvf_ctr_period(?, ?, ?, ?, ?)"
        columns = None
        data = None

        cursor = self.db_conn.cursor()
        cursor = cursor.execute(query, start_time, end_time, aggregation, device_type, product)
        if cursor:
            columns = [col[0] for col in cursor.description]
            data = cursor.fetchall()
        return columns, data

