import threading
import time
import os
import datetime

class file_scanner():

    def __init__(self):
        self.interval = 15
        self.device_types = self.get_devicetypes()
        self.file_set = {device: list(self.__load_fset(device)) for device in self.device_type}
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        while True:
            self.filescan()
            time.sleep(self.interval)

    def __load_fset(self, platform):
        with os.scandir(self.__data_dir.format(platform)) as dir:
            for file in dir:
                if file.is_dir() and file.name.upper() not in map(str.upper, self.device_type):
                    continue

                fobj, fpath, fdate, fext = self.__parse_filename(file)
                # If has a valid file name format, if it is of a processable extension and if correct object type
                if not fpath:
                    continue
                if fext not in self.ext_mapper.keys():
                    continue
                if not fobj == self.__obj_name:
                    continue

                yield (fpath, datetime.timestamp(fdate), fext)

    def filescan(self):
        for device in self.device_type:
            for file in self.__load_fset(device):
                if file in self.file_set:
                    continue
                else:
                    self.file_set[device].append(file)


    @staticmethod
    def __parse_filename(filepath):
        try:
            filename = filepath.name
            fsplit = filename.split(".")
            fpath = filepath.path
            fext = fsplit[1]
            fsplit = fsplit[0].split('_')
            fobj = fsplit[0]
            fdate = fsplit[1]
        # Not the files we are looking for.
        except Exception:
            return (None,)*4
        try:
            fdate = datetime.strptime(fdate, '%Y%m%d')
        except ValueError:
            # Not a valid date format, what you guys thinking
            return (None, )*4
        return fobj.upper(), fpath, fdate, fext.upper()