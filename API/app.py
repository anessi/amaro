from flask import Flask, request, jsonify, abort
from product_event import product_event
from flask import request
import pyodbc
import json
import sys
app = Flask("Amaro Events Processor")

def setup_database():
    db_user = 'sa'
    db_pwd = '123'
    db = 'CASE_AMARO'
    server = 'localhost'
    driver = '{ODBC DRIVER 17 for SQL Server}'
    port = '1433'
    con_str = "DRIVER={};SERVER={};PORT={};DATABASE={};UID={};PWD={}"
    con_str = con_str.format(driver, server, port, db, db_user, db_pwd)
    conn = pyodbc.connect(con_str)
    return conn
@app.route('/')
def index():
    return "Amaro Teste {}"

@app.route('/GET/client_ctr', methods=['GET'])
def get_client_ctr():
    start_time = request.args.get('startTimestamp')
    end_time = request.args.get('endTimestamp')
    aggregation = request.args.get('aggregation')
    product = request.args.get('product')
    device_type = request.args.get('platform')
    if not all([start_time, end_time, aggregation]):
        abort(400)
        
    fpath = 'D:\\Solution\\AMARO_CASE\\productEvent'
    prodEvent = product_event(fpath, db_con)
    ret = {}
    for device_type, device_ctr in prodEvent.get_ctr(start_time, end_time, aggregation, product, device_type):
        ret[device_type] = json.loads(device_ctr.to_json(orient="records", date_format='iso', date_unit='s'))
    response = jsonify(ret)
    response.status_code = 200
    return response

if __name__ == "__main__":
    db_con = setup_database()
    app.run(debug=False)