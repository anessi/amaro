USE CASE_AMARO

/* VIEW DE BASE PARA DEMAIS DELIVERABLES */
SELECT *
FROM vw_orders_retention
GO

/* Media de dias entre compras e nr de compras totais por usu�rio */
SELECT user_id, avg(day_diff) as avg_day_distance, max(order_cnt) qtd_orders
FROM vw_orders_retention
group by user_id
GO
DECLARE
	@start_time datetime = '2016-09-01'
	, @new_period int = 5
	, @return_period int = 2

/* Fun�ao de tabela para buscar taxas de retencao */
SELECT * FROM tvf_retention_rate(@start_time, @new_period, @return_period) ORDER BY Month_ref, Year_ref
GO

DECLARE 
	@start_time datetime
	, @end_time datetime
	, @aggregation int
	, @platform varchar(200)

SET @start_time = '2016-02-01 00:00:00'
SET @end_time = '2016-02-01 23:59:00'
SET @aggregation = 200

SET @platform = 'MobileWeb'
SELECT * 
FROM tvf_ctr_period(@start_time, @end_time, @aggregation, @platform, default)
ORDER BY nr_interval