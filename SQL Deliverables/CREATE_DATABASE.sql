--ALTER DATABASE CASE_AMARO SET OFFLINE WITH ROLLBACK IMMEDIATE
--DROP DATABASE CASE_AMARO
CREATE DATABASE [CASE_AMARO]
GO
USE CASE_AMARO

-- Criando tabela orders
CREATE TABLE [dbo].[orders](
	[id] bigint NOT NULL primary key,
	[user_id] [int] NOT NULL,
	[order_date] [datetime2](7) NOT NULL,
	[order_subtotal] [float] NOT NULL,
	[order_discount] [float] NOT NULL,
	[order_total] [float] NOT NULL,
	[order_status] [nvarchar](100) NOT NULL,
	[payment_type] [nvarchar](100) NOT NULL,
	[shipping_cost] [float] NOT NULL,
	[shipping_service] [nvarchar](100) NOT NULL,
	[address_city] [nvarchar](100) NOT NULL,
	[address_state] [nvarchar](100) NOT NULL,
	[utm_source_medium] [nvarchar](100) NOT NULL,
	[device_type] [nvarchar](100) NOT NULL
) ON [PRIMARY]

GO
-- Criando Tabela Order_items
CREATE TABLE [dbo].[order_items](
	[id] [bigint] primary key,
	[order_id] [bigint] foreign key references orders(id),
	[sku] [varchar](100) NULL,
	[quantity] [int],
	[code_color] [varchar](100) NULL
) ON [PRIMARY]
GO
CREATE VIEW [dbo].[vw_orders_retention] AS 
SELECT 
	ord.id, ord.user_id
	, ord.order_date
	, COUNT(1) OVER (PARTITION BY ord.user_id ORDER BY ord.order_date ASC) AS order_cnt
	, DATEDIFF(D, LAG(ord.order_date,1,NULL) OVER (PARTITION BY ord.user_id ORDER BY ord.order_date ASC), ord.order_date)  as day_diff
	, ord.order_subtotal
	, ord.order_discount
	, ord.order_total
	, ord.order_status
	, ord.payment_type
	, ord.shipping_cost
	, ord.shipping_service
	, ord.address_city
	, ord.address_state
	, ord.utm_source_medium
	, ord.device_type
FROM orders ord
GO
CREATE FUNCTION [dbo].[tvf_retention_rate]
(	-- Between 5 and 2 AND between 2 and 0 / between 5 and 2
	@date_ref date=null,
	@months_new int,
	@months_ret int)
RETURNS TABLE 
AS
RETURN 
(
	WITH NEW_CUST AS (
	SELECT ord.user_id, ord.id, ord.order_date, ord.order_cnt
	FROM vw_orders_retention ord
	WHERE 
		ord.order_date BETWEEN 
						DATEADD(DAY,1,EOMONTH(CAST(DATEADD(MONTH, @months_new * -1, COALESCE(@date_ref,GETDATE())) AS DATE)))
						AND EOMONTH(DATEADD(M, @months_ret * -1,DATEADD(D,1,EOMONTH(COALESCE(@date_ref,GETDATE())))),-1)
		AND ord.order_cnt = 1
	)
	, RETURNING_CUST AS (
	SELECT ord.user_id, ord.id, ord.order_date
	FROM vw_orders_retention ord
	WHERE
		ord.order_date BETWEEN 
						DATEADD(DAY,1,EOMONTH(DATEADD(M,@months_ret * -1,DATEADD(D,1,EOMONTH(COALESCE(@date_ref,GETDATE())))),-1))
						AND EOMONTH(COALESCE(@date_ref,GETDATE()))
		AND ord.order_cnt > 1
	)
	, RETENTIONS AS (
	SELECT 
		SUM(CASE WHEN new.user_id IS NOT NULL
				AND ret.user_id IS NOT NULL
				THEN 1
				ELSE 0
			END) AS month_total_return
		, COUNT(new.user_id) AS month_total_new
		, MONTH(new.order_date) AS Month_ref, YEAR(new.order_date) AS Year_ref
	FROM 
		NEW_CUST new
		LEFT JOIN RETURNING_CUST ret ON new.user_id = ret.user_id
	GROUP BY MONTH(new.order_date), YEAR(new.order_date)
	)
	, RETENTION_CALC AS (
	SELECT 
		Month_ref, Year_ref
		, month_total_return, month_total_new
		, month_total_return * 100.0 / month_total_new AS month_retention
		, SUM(month_total_return) OVER (ORDER BY Month_ref, Year_ref ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS rolling_return
		, SUM(month_total_new) OVER (ORDER BY Month_ref, Year_ref ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS rolling_new
		, SUM(month_total_return) OVER (ORDER BY Month_ref, Year_ref ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) *100.0
			/ SUM(month_total_new) OVER (ORDER BY Month_ref, Year_ref ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS rolling_retention
		
	FROM RETENTIONS
	)
	SELECT *
	FROM RETENTION_CALC
)
GO

CREATE TABLE Tally(n int not null)
GO

CREATE FUNCTION [dbo].[tvf_ctr_period]
(	
	@start_time datetime
	, @end_time datetime
	, @aggregation int
	, @platform varchar(200) = NULL
	, @product varchar(200) = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	WITH time_intervals AS (
	SELECT 
		n AS nr_interval,
		DATEADD(MINUTE, @aggregation * (n-1), @start_time) start_interval,
		CASE WHEN DATEADD(MINUTE, @aggregation * (n), @start_time) > @end_time
			THEN @end_time
			ELSE DATEADD(MINUTE, @aggregation * (n), @start_time)
		END AS end_interval
	FROM Tally 
	WHERE
		n <= (DATEDIFF(MINUTE, @start_time, @end_time) / @aggregation)+1)
		SELECT 
			COUNT(DISTINCT ord.id) AS nr_orders, 
			items.code_color,
			ord.device_type,
			ints.nr_interval, 
			ints.start_interval, ints.end_interval
		FROM 
			orders ord
			INNER JOIN time_intervals ints ON 
				ord.order_date >= ints.start_interval
				AND ord.order_date < ints.end_interval
			INNER JOIN order_items items ON ord.id = items.order_id
		WHERE
			(@platform IS NULL OR device_type = @platform)
			AND (@product IS NULL OR items.code_color = @product)
		GROUP BY 
			ints.nr_interval
			, ints.start_interval, ints.end_interval
			, ord.device_type
			, items.code_color
)
GO


WITH _Tally (n) AS
(
    -- 1000 rows
    SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL))
    FROM (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) a(n)
    CROSS JOIN (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) b(n)
    CROSS JOIN (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) c(n)
	CROSS JOIN (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) d(n)
	CROSS JOIN (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) e(n)
	CROSS JOIN (VALUES(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)) f(n)
)
INSERT INTO Tally 
SELECT *
FROM _Tally;
GO
SET DATEFORMAT DMY
BULK INSERT dbo.orders FROM 'D:\Repo\AMARO_CASE\orders.csv' WITH (FIRSTROW=2, FIELDTERMINATOR = ';', ROWTERMINATOR = '\n') 
BULK INSERT dbo.order_items FROM 'D:\Repo\AMARO_CASE\order_items.csv' WITH (FIRSTROW=2, FIELDTERMINATOR = ';', ROWTERMINATOR = '\n') 

